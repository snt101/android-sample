package com.example.singlesawareness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.*;


import com.facebook.*;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class SelectionFragment extends Fragment {

	private static final String TAG = "SelectionFragment";
	private ProfilePictureView profilePictureView;
	private TextView userNameView;
	private TextView welStatement;
	private static final List<String> PERMISSIONS = Arrays.asList("user_checkins", "user_likes", "user_friends", "friends_location", "friends_checkins",  "friends_notes", "friends_likes");
	ArrayList<PageInfo> pages = new ArrayList<PageInfo>();
    
    private static final Location CSUCI_LOCATION = new Location("") {
        {
            setLatitude(34.161894);
            setLongitude(-119.043136);
        }
    };
    
    private static final Location HOUSE_LOCATION = new Location("") {
        {
            setLatitude(34.171859);
            setLongitude(-118.952605);
        }
    };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.selection, container, false);

		// Find the user's profile picture custom view
		profilePictureView = (ProfilePictureView) view
				.findViewById(R.id.selection_profile_pic);
		profilePictureView.setCropped(true);

		// Find the user's name view
		userNameView = (TextView) view.findViewById(R.id.selection_user_name);

		// Check for an open session
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			// Get the user's data
			makeMeRequest(session);
		}

		// statements
		welStatement = (TextView) view.findViewById(R.id.statement);
		welStatement.setText("Please select between the two genders:");

		// buttons
		 Button maleButton;
		 Button femaleButton;
		maleButton = (Button) view.findViewById(R.id.button_male);
		maleButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Do something in response to button click
				String latitude = Double.toString(HOUSE_LOCATION.getLatitude());
				String longitude = Double.toString(HOUSE_LOCATION.getLongitude());
				
				//FQL statement
				String fqlQuery = "{" +
				"'placesnearby': 'SELECT page_id " +
						"FROM place WHERE" + " distance" + "(" + "latitude, longitude, " +
						"\""  + latitude + "\"" + "," + "\"" + longitude + "\") < 1000 " +
						"LIMIT 10'," +
				"'placeinfo': 'select page_id, location, name, pic, about from page where page_id in " +
						"(SELECT page_id from #placesnearby)'," +
				"'idfromcheckin': ' SELECT author_uid from checkin where target_id in " +
						"(select page_id from #placeinfo)',"  +
				"'idfromlikes': ' SELECT uid FROM page_fan WHERE uid IN "	+
						"(SELECT uid2 FROM friend WHERE uid1 = me()) AND " +
						"(page_id in (select page_id from #placeinfo))'," +
		        "'checkingender': ' SELECT sex  from user where uid in (select author_uid from #idfromcheckin)'," +
		        "'likegender': ' SELECT sex  from user where uid in (select uid from #idfromlikes)'," +						
						"}";
		        Bundle params = new Bundle();
		        params.putString("q", fqlQuery);
		        Session session = Session.getActiveSession();
		        Request request = new Request(session,
		            "/fql",                         
		            params,                         
		            HttpMethod.GET,                 
		            new Request.Callback(){         
		                public void onCompleted(Response response) {
		                    Log.i(TAG, "Result: " + response.toString());
		                    pages = new ArrayList<PageInfo>();
		                 try {
		                     GraphObject go = response.getGraphObject();
		                     JSONObject jso = go.getInnerJSONObject();
		                     

		                     //array of placeinfo query
		                    
		                     JSONArray placeInfo = jso.getJSONArray("data").getJSONObject(1)
		                             .getJSONArray("fql_result_set");
		                     JSONArray checkInfo = jso.getJSONArray("data").getJSONObject(4)
		                             .getJSONArray("fql_result_set");
		                     JSONArray likeInfo = jso.getJSONArray("data").getJSONObject(4)
		                             .getJSONArray("fql_result_set");
		                     for(int i =0; i <10; i++)
		                     {
		                    	 PageInfo temp = new PageInfo();
		                    	 JSONObject placeInfoData = placeInfo.getJSONObject(i);
		                    	 
		                    	 //gets pageid
		                    	 temp.setPageID(placeInfoData.getString( "page_id" ));
		                    	 
		                    	 //location
		                    	 JSONObject location = placeInfoData.getJSONObject("location");
		                    	 temp.setCity(location.getString("city"));
		                    	 temp.setstreet(location.getString("street"));
		                    	 temp.setState(location.getString("state"));
		                    	 temp.setZip(location.getString("zip"));
		                    	 
		                    	 //name
		                    	 temp.setPageName(placeInfoData.getString("name"));
		                    	 
		                    	 //url
		                    	 temp.setPictureURL(placeInfoData.getString("pic"));
		                    	 
		                    	 //about
		                    	 temp.setAbout(placeInfoData.getString("about"));
		                    	 
		                    	 int him=0;
			                     int her=0;
			                     
			                     
			                     for(int l=0; l <checkInfo.length();l++)
			                     {
			                     JSONObject checkData = checkInfo.getJSONObject(l);
			                     String tempString = checkData.getString("sex");
			                     
			                     if(tempString.equalsIgnoreCase("male"))
			                     {
			                    	 him++;
			                     }
			                     else if(tempString.equalsIgnoreCase("female"))
			                    		 {
			                    	 her++;
			                    		 }
			                     
			                     Log.i(TAG, "Sex: " + tempString);
			                     
			                     }
			                     for(int j=0; j <likeInfo.length();j++)
			                     {
			                     JSONObject likeData = likeInfo.getJSONObject(j);
			                     String tempString = likeData.getString("sex");
			                     
			                     if(tempString.equalsIgnoreCase("male"))
			                     {
			                    	 him++;
			                     }
			                     else if(tempString.equalsIgnoreCase("female"))
			                    		 {
			                    	 her++;
			                    		 }
			                     
			                     Log.i(TAG, "Sex: " + tempString);
			                     
			                     }
		                    	 temp.setMales(Integer.toString(him));
		                    	 temp.setFemales(Integer.toString(her));
		                    	 pages.add(temp);

		                    	 Log.i(TAG, "Result: " + pages.get(i).getPageName());
		                     }
		                     
		                     
		                     
 
		                 }catch (JSONException e) {
		                     e.printStackTrace();
		                 }
		                }                  
		        }); 
		        Request.executeBatchAsync(request); 
		        
		        Intent data = new Intent(getActivity(), MaleActivity.class);
		        Bundle bundle = new Bundle();  
		        bundle.putParcelableArrayList("pageList", pages);
		        data.putExtras(bundle);
		        

		        startActivity(data);



			}
		});

		femaleButton = (Button) view.findViewById(R.id.button_female);
		femaleButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Do something in response to button click
				// Do something in response to button click
				String latitude = Double.toString(HOUSE_LOCATION.getLatitude());
				String longitude = Double.toString(HOUSE_LOCATION.getLongitude());
				
				//FQL statement
				String fqlQuery = "{" +
				"'placesnearby': 'SELECT page_id " +
						"FROM place WHERE" + " distance" + "(" + "latitude, longitude, " +
						"\""  + latitude + "\"" + "," + "\"" + longitude + "\") < 1000 " +
						"LIMIT 10'," +
				"'placeinfo': 'select page_id, location, name, pic, about from page where page_id in " +
						"(SELECT page_id from #placesnearby)'," +
				"'idfromcheckin': ' SELECT author_uid from checkin where target_id in " +
						"(select page_id from #placeinfo)',"  +
				"'idfromlikes': ' SELECT uid FROM page_fan WHERE uid IN "	+
						"(SELECT uid2 FROM friend WHERE uid1 = me()) AND " +
						"(page_id in (select page_id from #placeinfo))'," +
		        "'checkingender': ' SELECT sex  from user where uid in (select author_uid from #idfromcheckin)'," +
		        "'likegender': ' SELECT sex  from user where uid in (select uid from #idfromlikes)'," +						
						"}";
		        Bundle params = new Bundle();
		        params.putString("q", fqlQuery);
		        Session session = Session.getActiveSession();
		        Request request = new Request(session,
		            "/fql",                         
		            params,                         
		            HttpMethod.GET,                 
		            new Request.Callback(){         
		                public void onCompleted(Response response) {
		                    Log.i(TAG, "Result: " + response.toString());
		                    pages = new ArrayList<PageInfo>();
		                 try {
		                     GraphObject go = response.getGraphObject();
		                     JSONObject jso = go.getInnerJSONObject();
		                     

		                     //array of placeinfo query
		                    
		                     JSONArray placeInfo = jso.getJSONArray("data").getJSONObject(1)
		                             .getJSONArray("fql_result_set");
		                     JSONArray checkInfo = jso.getJSONArray("data").getJSONObject(4)
		                             .getJSONArray("fql_result_set");
		                     JSONArray likeInfo = jso.getJSONArray("data").getJSONObject(4)
		                             .getJSONArray("fql_result_set");
		                     for(int i =0; i <10; i++)
		                     {
		                    	 PageInfo temp = new PageInfo();
		                    	 JSONObject placeInfoData = placeInfo.getJSONObject(i);
		                    	 
		                    	 //gets pageid
		                    	 temp.setPageID(placeInfoData.getString( "page_id" ));
		                    	 
		                    	 //location
		                    	 JSONObject location = placeInfoData.getJSONObject("location");
		                    	 temp.setCity(location.getString("city"));
		                    	 temp.setstreet(location.getString("street"));
		                    	 temp.setState(location.getString("state"));
		                    	 temp.setZip(location.getString("zip"));
		                    	 
		                    	 //name
		                    	 temp.setPageName(placeInfoData.getString("name"));
		                    	 
		                    	 //url
		                    	 temp.setPictureURL(placeInfoData.getString("pic"));
		                    	 
		                    	 //about
		                    	 temp.setAbout(placeInfoData.getString("about"));
		                    	 
		                    	 int him=0;
			                     int her=0;
			                     
			                     
			                     for(int l=0; l <checkInfo.length();l++)
			                     {
			                     JSONObject checkData = checkInfo.getJSONObject(l);
			                     String tempString = checkData.getString("sex");
			                     
			                     if(tempString.equalsIgnoreCase("male"))
			                     {
			                    	 him++;
			                     }
			                     else if(tempString.equalsIgnoreCase("female"))
			                    		 {
			                    	 her++;
			                    		 }
			                     
			                     Log.i(TAG, "Sex: " + tempString);
			                     
			                     }
			                     for(int j=0; j <likeInfo.length();j++)
			                     {
			                     JSONObject likeData = likeInfo.getJSONObject(j);
			                     String tempString = likeData.getString("sex");
			                     
			                     if(tempString.equalsIgnoreCase("male"))
			                     {
			                    	 him++;
			                     }
			                     else if(tempString.equalsIgnoreCase("female"))
			                    		 {
			                    	 her++;
			                    		 }
			                     
			                     Log.i(TAG, "Sex: " + tempString);
			                     
			                     }
		                    	 temp.setMales(Integer.toString(him));
		                    	 temp.setFemales(Integer.toString(her));
		                    	 pages.add(temp);

		                    	 Log.i(TAG, "Result: " + pages.get(i).getPageName());
		                     }  
 
		                 }catch (JSONException e) {
		                     e.printStackTrace();
		                 }
		                }                  
		        }); 
		        Request.executeBatchAsync(request);

		        Intent data = new Intent(getActivity(), MaleActivity.class);
		        data.putParcelableArrayListExtra("page", pages);
		        
		        ArrayList<PageInfo> tempPages = new ArrayList<PageInfo>();
		        
		        if (data != null) {

	            	Log.i("DATA:", "!NUll");

	            }
		        startActivity(data);
			}
		});

		return view;
	}

	private void makeMeRequest(final Session session) {
		// Make an API call to get user data and define a
		// new callback to handle the response.
        session.requestNewReadPermissions(new Session.NewPermissionsRequest(SelectionFragment.this, PERMISSIONS));
		Request request = Request.newMeRequest(session,
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						// If the response is successful
						if (session == Session.getActiveSession()) {
							if (user != null) {
								// Set the id for the ProfilePictureView
								// view that in turn displays the profile
								// picture.
								profilePictureView.setProfileId(user.getId());
								// Set the Textview's text to the user's name.
								userNameView.setText("Welcome "
										+ user.getFirstName() + "!");
							}
						}
						if (response.getError() != null) {
							// Handle errors, will do so later.
						}
					}
				});
		request.executeAsync();
	}

	private void onSessionStateChange(final Session session,
			SessionState state, Exception exception) {
		if (session != null && session.isOpened()) {
			// Get the user's data.
			makeMeRequest(session);
		}
	}

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	private static final int REAUTH_ACTIVITY_CODE = 100;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REAUTH_ACTIVITY_CODE) {
			uiHelper.onActivityResult(requestCode, resultCode, data);
			
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		uiHelper.onSaveInstanceState(bundle);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


		
        
 /*       for(int i =0; i < pages.length; i++)
        {
        	String tempID = pages[i].getPageID();
        Bundle params1 = new Bundle();
        String fqlQuery1 ="{"+

        		"}";
        params.putString("q", fqlQuery1);
        Request request1 = new Request(session,
                "/fql",                         
                params1,                         
                HttpMethod.GET,                 
                new Request.Callback(){         
                    public void onCompleted(Response response) {
                        Log.i(TAG, "Result: " + response.toString());
                    }                  
            }); 
            Request.executeBatchAsync(request1);
        }*/
        
        
public ArrayList<PageInfo> getPages()
{
return	this.pages;
}
		
	
}
