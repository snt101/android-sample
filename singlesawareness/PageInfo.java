package com.example.singlesawareness;

import android.os.Parcel;
import android.os.Parcelable;

public class PageInfo implements Parcelable{
	private String PageID;
	private String about;
	private String pictureURL;
	private String PageName;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String males;
	private String females;
	
	public PageInfo()
	{
	    PageID = "";
		PageName ="";
		about="";
		street = "";
		pictureURL = "";
		city="";
		state="";
		zip="";
		males="";
		females="";
		
	}
	public PageInfo(Parcel in)
	{
		String[] data = new String[10];
	    this.PageID = in.readString();
		this.PageName =  in.readString();
		this.about = in.readString();
		this.street = in.readString();
		this.pictureURL = in.readString();
		this.city = in.readString();
		this.state=in.readString();
		this.zip=in.readString();
		this.males=in.readString();
		this.females=in.readString();
		
	}

	@Override
    public int describeContents(){
        return 0;
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.PageID); 
        dest.writeString(this.PageName);
        dest.writeString(this.about);
        dest.writeString(this.street);
        dest.writeString(this.pictureURL);
        dest.writeString(this.city); 
        dest.writeString(this.state);
        dest.writeString(this.zip); 	
        dest.writeString(this.males); 
        dest.writeString(this.females);
    }
    
    @SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public PageInfo createFromParcel(Parcel in) {
            return new PageInfo(in); 
        }

        public PageInfo[] newArray(int size) {
            return new PageInfo[size];
        }
    };

	
	public String getPageID()
	{
	return this.PageID;
	}
	
	public String getAbout(){
		return this.about;
	}
	
	public void setAbout(String about){
		this.about = about;
	}
	
	public String getpictureURL()
	{
	return this.pictureURL;
	}
	
	public void setPictureURL(String url)
	{
		this.pictureURL = url;
	}
	public String getPageName()
	{
	return this.PageName;
	}
	
	
	public String getstreet()
	{
	return this.street;
	}
	
	public String getCity()
	{
	return this.city;
	}
	
	public String getState()
	{
	return this.state;
	}
	
	public String getZip()
	{
	return this.zip;
	}
	
	public String getMales()
	{
	return this.males;
	}
	
	public String getFemales()
	{
	return this.females;
}
	
	public void setPageID(String PageID)
	{
	this.PageID = PageID;
	}
	
	public void setPageName(String PageName)
	{
	this.PageName = PageName;
	}
	
	
	public void setstreet(String street)
	{
	this.street = street;
	}
	
	public void setCity(String city)
	{
	this.city = city;
	}
	
	public void setState(String State)
	{
	this.state = State;
	}
	
	public void setZip(String zip)
	{
	this.zip = zip;
	}
	
	public void setMales(String males)
	{
		this.males = males;
	}
	
	public void setFemales(String females)
	{
	this.females = females;
	}
	
}
